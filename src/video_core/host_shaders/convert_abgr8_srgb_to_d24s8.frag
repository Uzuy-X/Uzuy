// SPDX-License-Identifier: GPL-2.0-or-later

#version 450
#extension GL_ARB_shader_stencil_export : require

precision highp float;
precision mediump int;

layout(binding = 0) uniform sampler2D color_texture;

const float SRGB_THRESHOLD = 0.04045;
const float SRGB_INV_GAMMA = 1.0 / 12.92;
const float SRGB_GAMMA = 2.4;
const float EXP24_MINUS_1 = 16777215.0; // 2^24 - 1
const float EXP8_MINUS_1 = 255.0; // 2^8 - 1

// Utility function to convert sRGB to linear space
vec3 srgbToLinear(vec3 srgb) {
    return mix(srgb * SRGB_INV_GAMMA, pow((srgb + 0.055) / 1.055, vec3(SRGB_GAMMA)), step(SRGB_THRESHOLD, srgb));
}

void main() {
    ivec2 coord = ivec2(gl_FragCoord.xy);

    // Fetch sRGB color and convert to linear space
    vec4 srgbColor = texelFetch(color_texture, coord, 0);
    vec3 linearColor = srgbToLinear(srgbColor.rgb);

    // Compute luminance for depth using standard luminance coefficients
    float luminance = dot(linearColor, vec3(0.2126, 0.7152, 0.0722));
    uint depth_val = uint(luminance * EXP24_MINUS_1);

    // Extract the stencil value from the alpha component
    uint stencil_val = uint(srgbColor.a * EXP8_MINUS_1);

    // Pack stencil and depth values into a single uint
    uint depth_stencil_unorm = (stencil_val << 24) | (depth_val & 0x00FFFFFFu);

    // Set depth and stencil values for the fragment
    gl_FragDepth = float(depth_stencil_unorm & 0x00FFFFFFu) / EXP24_MINUS_1;
    gl_FragStencilRefARB = int(depth_stencil_unorm >> 24);
}
