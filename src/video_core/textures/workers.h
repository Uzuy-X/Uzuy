// SPDX-FileCopyrightText: Copyright 2023 uzuy Emulator Project
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "common/thread_worker.h"  // Include for Common::ThreadWorker

namespace Tegra::Texture {

// Function declaration for getting a reference to the static thread workers
Common::ThreadWorker& GetThreadWorkers();

} // namespace Tegra::Texture
