// SPDX-FileCopyrightText: Copyright 2023 uzuy Emulator Project
// SPDX-License-Identifier: GPL-2.0-or-later

#include "video_core/textures/workers.h"

namespace Tegra::Texture {

Common::ThreadWorker& GetThreadWorkers() {
    // Ensure we have at least 1 thread
    const unsigned int hardware_threads = std::max(std::thread::hardware_concurrency(), 2U);
    const unsigned int thread_count = std::max(hardware_threads / 2, 1U);

    // Create or return the static worker pool with appropriate thread count
    static Common::ThreadWorker workers{thread_count, "ImageTranscode"};
    return workers;
}

} // namespace Tegra::Texture
